import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'

let MAX_PLAYER = 3
var now_player = 0
var ip2name = {}
var CHARACTER = {
  name: 'Bird',
  role: 'Human',
  leader: false,
  sex: 1
}

var config = require('./webpack.config');
var webpack = require('webpack')
var app = express();
var server = require('http').Server(app);
var compiler = webpack(config);
var io = require('socket.io')(server)

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  noInfo: true,
  quiet: false
}));

app.use(require('webpack-hot-middleware')(compiler));
app.use( bodyParser.json() );
app.use(express.static('static'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.post('/', function(req, res) {
  console.log(req.body)
  ip2name[req.ip] = req.body.nickName
  console.log(ip2name)
  res.sendStatus(200)
});

io.on('connection', function (socket) {
  // console.log(socket.id)
  // console.log(socket.handshake.address)
  console.log(CHARACTER)
  setTimeout(() => {
    var name = ip2name[socket.handshake.address]
    socket.emit('wait', {
      name: name,
      player: 1
    })
  }, 2000)

  socket.on('chat', (text) => {
    // console.log(socket.id)
    // console.log(text)
    io.sockets.emit('chat', text)
  })
})

server.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
})
