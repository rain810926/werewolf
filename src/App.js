import React, { Component } from 'react';
import './App.scss'

export default class App extends Component {

  render() {

    return (
      <div className={`appWrap`}>
        {this.props.children}
      </div>
    );
  }
}
