import React from 'react';
import { Router, Route, hashHistory, IndexRoute, browserHistory } from 'react-router'
import ReactDOM from 'react-dom';
import Entrance from './routes/Entrance';
import Lobby from './routes/Lobby';
import App from './App'

// ReactDOM.render(<App />, document.getElementById('root'));

ReactDOM.render((
  <Router history={browserHistory}>
    <Route path='/' component={App}>
      <IndexRoute component={Entrance} />
      <Route path='lobby/:roomId/:nickName' component={Lobby} >
      </Route>
    </Route>
  </Router>

), document.getElementById('root'))
