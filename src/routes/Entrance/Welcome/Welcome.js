import React, {Component} from 'react'
import { Link, browserHistory } from 'react-router'
import './Welcome.scss'
import 'whatwg-fetch'

export default class Welcome extends Component {

  constructor() {
    super()
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit(e) {
    e.preventDefault();
    fetch('/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        nickName: this.input.value,
      })
    }).then((res) => {
      if(res.status === 200) {
        return res.json()
      } else if(res.status === 301) {
        alert('名字太短了')
        return null
      } else if(res.status === 302) {
        alert('名字太長了')
        return null
      }
    }).then((json) => {
      if(json) {
        browserHistory.push(`/lobby/${json.room}/${this.input.value}`)
      }
    })

  }

  render() {
    return (
        <div className={`container-fluid welcomeWrap`}>
          <div className={`title`}>
            <h1>Werewolf</h1>
            <div className={`formWrap`}>
              <form className={`form-inline`} onSubmit={this.handleSubmit}>
                  <input
                    id="input"
                    className={`form-control inputStyle`}
                    type="text"
                    placeholder="What's your name?"
                    ref={(input) => this.input = input}
                  />
                {/*<Link to='/village'>*/}
                <button className={`btn btn-outline-success`} type="submit">
                  <i className="fa fa-eercast" aria-hidden="true"></i>
                </button>
              </form>
            </div>
          </div>
        </div>
    )
  }
}
