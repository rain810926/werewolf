import React, { Component } from 'react';
import Welcome from './Welcome'
import './Entrance.scss'

export default class Entrance extends Component {
  render() {
    return (
      <div className={`werewolf`}>
        <Welcome />
      </div>
    );
  }
}
