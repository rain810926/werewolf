import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router'
import './Lobby.scss'
import Interface from './Interface'
import Chat from './Chat'
import System from './System'

export default class Lobby extends Component {
  constructor() {
    super()
    this.state = {
      socket: null,
      hour: 'day',
      gameover: null,
      poster: false
    }
    this.handleClick = this.handleClick.bind(this)
    this.handlePoster = this.handlePoster.bind(this)
  }

  handleClick() {
    browserHistory.push(`/`)
  }

  handlePoster(messages) {
    var poster = document.getElementById('poster')
    poster.innerHTML = ''
    poster.className += 'poster'
    for(var index in messages) {
      var h1 = document.createElement('h1')
      h1.innerHTML = messages[index]
      poster.appendChild(h1)
    }
    this.setState({
      poster: true
    })
    setTimeout(() => {
      this.setState({
        poster: false
      })
    }, 8500)
  }

  componentWillMount() {
    var that = this
    var socket = io.connect('/' + this.props.params.roomId)
    that.setState({
      socket: socket
    })

  }

  componentWillUnmount() {
    this.state.socket.disconnect()
  }

  componentDidMount() {
    this.state.socket.emit('come', this.props.params.nickName)

    this.state.socket.on('night', () => {
      this.setState({
        hour: 'night'
      })
    })

    this.state.socket.on('day', () => {
      this.setState({
        hour: 'day'
      })
    })

    this.state.socket.on('gameover', (winer) => {
      this.state.socket.removeAllListeners('bigPoster')
      this.setState({
        gameover: winer
      })
    })

    this.state.socket.on('bigPoster', (messages) => {
      this.handlePoster(messages)
    })
  }

  render() {
    if(this.state.hour === 'day') {
      var style = 'day'
    } else {
      var style = 'night'
    }

    if(this.state.poster) {
      var posterDisplay = 'posterDisplay'
    } else {
      var posterDisplay = 'none'
    }

    return (
      <div className={`container lobbyWrap ${style}`}>
        <Interface
          socket={this.state.socket}
          userName={this.props.params.nickName}
        />
        <Chat
          socket={this.state.socket}
        />
        <div className={`screen ${style}`} />

        {
          this.state.gameover ?
          <div className={`gameover`}>
            <h1>GAMEOVER</h1>
            <h1>{this.state.gameover}獲勝</h1>
            <span
              className={`leave`}
              onClick={() => this.handleClick()}
            >
            Click to leave
            </span>
          </div>
        : null
        }

        <div id='poster' className={`${posterDisplay}`}>

        </div>
      </div>
    )
  }
}
