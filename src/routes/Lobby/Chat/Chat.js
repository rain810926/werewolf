import React, { Component } from 'react';
import { Link } from 'react-router'
import './Chat.scss'

export default class Chat extends Component {
  constructor() {
    super()
    this.state = {
      userStatus: 'live',
      timestamp: 0,
      selectBar: false
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleMessage = this.handleMessage.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleMessage(owner, text, died, target) {
    var messageWrap = document.createElement('div')
    messageWrap.className += 'row'
    if(died) {
      messageWrap.className += ' diedMessage'
    }
    var message = document.createElement('span')
    message.innerHTML = text
    var speaker = document.createElement('p')
    speaker.innerHTML = owner
    messageWrap.appendChild(speaker)
    messageWrap.appendChild(message)
    target.appendChild(messageWrap)
    target.scrollTop = target.scrollHeight
  }

  componentDidMount () {
    var messages = document.getElementById('messages')
    var system = document.getElementById('system')

    this.props.socket.on('chat', (data) => {
      if('died' in data) {
        this.handleMessage(data['owner'], data['text'], true, messages)
      } else {
        this.handleMessage(data['owner'], data['text'], false, messages)
      }
    })

    this.props.socket.on('aside', (text) => {
      this.handleMessage('Aside', text, false, system)
    })

    this.props.socket.on('transform', () => {
      messages.innerHTML = ''
      system.innerHTML = ''
    })

    this.props.socket.on('died', () => {
      this.setState({
        userStatus: 'died'
      })
    })
  }

  handleSubmit(e, type) {
    e.preventDefault();
    if(this.input.value.length === 0) {
      return
    }
    if(Date.now() - this.state.timestamp < 1000) {
      var system = document.getElementById('system')
      this.input.value = ''
      var messageWrap = document.createElement('div')
      messageWrap.className += 'row'
      var message = document.createElement('span')
      message.innerHTML = '你的發言太頻繁了'
      var speaker = document.createElement('p')
      speaker.innerHTML = 'Aside'
      messageWrap.appendChild(speaker)
      messageWrap.appendChild(message)
      system.appendChild(messageWrap)
      system.scrollTop = system.scrollHeight
      return
    }
    switch (type) {
      case 'normal':
        this.props.socket.emit('chat', this.input.value)
        break;
      case 'louder':
        var h1 = '<h1>' + this.input.value + '</h1>'
        this.props.socket.emit('chat', h1)
        break;
      case 'image':
        var img = '<img src="' + this.input.value + '" />'
        this.props.socket.emit('chat', img)
        break;
    }
    this.input.value = ''
    this.setState({
      timestamp: Date.now()
    })
  }

  handleChange() {
    if(this.input.value.slice(-1)[0] === '@') {
      this.setState({
        selectBar: true
      })
    } else {
      this.setState({
        selectBar: false
      })
    }
  }

  render() {
    if(this.state.userStatus === 'died') {
      var placeholder = 'You are died ...'
    } else {
      var placeholder = 'You are alive ...'
    }

    return (
      <div className={`col-md-12 chatWrap`}>
        {/*<Character />*/}

        <div
          className={`jumbotrun`}
          style={{height: '85%'}}
        >
          <ul id="system" className={`col-md-3`} />
          <ul id="messages" className={`col-md-9`} />
        </div>

        {/*send block*/}
        <form className={`formWrap`} onSubmit={(e) => this.handleSubmit(e, 'normal')}>
          <input
            id="input"
            className={`form-control inputStyle`}
            type="text"
            autoComplete='off'
            placeholder={placeholder}
            ref={(input) => this.input = input}
            onChange={this.handleChange}
          />
          <div className={`speakerGroup`}>
            <i
              className="fa fa-bullhorn"
              aria-hidden="true"
              onClick={(e) => this.handleSubmit(e, 'louder')}
            />
            <i
              className="fa fa-picture-o"
              aria-hidden="true"
              onClick={(e) => this.handleSubmit(e, 'image')}
            />
          </div>
        </form>

      </div>
    )
  }
}
