import React, { Component } from 'react';
import './Village.scss'
import Villager from './Villager'

export default class Village extends Component {
  constructor() {
    super()
    this.state = {
      users: null,
      interval: 0,
    }

    this.timeCount = null
    this.reciprocal = this.reciprocal.bind(this)

  }

  componentDidMount() {
    this.props.socket.on('updateAll', (data) => {
      delete data[this.props.userID]
      var tmpArray = []
      for(var key in data) {
        var tmpDict = {
          socketId: key,
          user: data[key]
        }
        tmpArray.push(tmpDict)
      }
      this.setState({
        users: tmpArray
      })
    })

    this.props.socket.on('time', (interval) => {
      clearInterval(this.timeCount)
      this.setState({
        interval: interval,
      })

      this.timeCount = setInterval(this.reciprocal, 1000)
    })
  }

  reciprocal() {
    if(this.state.interval === 0) {
      clearInterval(this.timeCount)
    } else {
      this.setState({
        interval: this.state.interval - 1
      })
    }
  }

  render() {
    return (
      <div className={`villageWrap container`}>
        {
          this.state.users ?
          this.state.users.map((userDict, index) => (
            <Villager
              userRole={this.props.userRole}
              key={index}
              user={userDict.user}
              socket={this.props.socket}
              socketId={userDict.socketId}
              computer={this.state.users[0]['user'].status}
            />
          ))
          : null
        }
        <div className={`timeBar`}>
          <div>
            <i className="fa fa-clock-o" aria-hidden="true"></i>
            &nbsp;&nbsp;
            {
              this.state.interval ? this.state.interval : null
            }
          </div>
        </div>
      </div>
    )
  }
}
