import React, { Component } from 'react';
import './Villager.scss'

export default class Villager extends Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)

    this.state = {
      vote: false,
      bite: false,
      guard: false,
      divine: false,
      gameover: false
    }
  }

  componentDidMount() {

    this.props.socket.on('vote', (state) => {
      this.setState({
        vote: state
      })
    })
    this.props.socket.on('bite', (state) => {
      this.setState({
        bite: state
      })
    })
    this.props.socket.on('guard', (state) => {
      this.setState({
        guard: state
      })
    })
    this.props.socket.on('divine', (state) => {
      this.setState({
        divine: state
      })
    })

    this.props.socket.on('gameover', () => {
      this.props.socket.removeAllListeners('aside')
      this.setState({
        gameover: true
      })
    })

  }

  handleClick(type) {

    switch (type) {
      case 1:
        this.props.socket.emit('vote', this.props.socketId)
        break
      case 2:
        this.props.socket.emit('bite', this.props.socketId)
        break
      case 3:
        this.props.socket.emit('divine', this.props.socketId)
        break
      case 4:
        this.props.socket.emit('guard', this.props.socketId)
        break
    }
  }

  render() {

    if(this.props.user.sex != null) {
      if(this.props.user.sex) {
        var gender = (<i className="fa fa-mars" aria-hidden="true" style={{color: '#2626a6'}} />)
      } else {
        var gender = (<i className="fa fa-venus" aria-hidden="true" style={{color: 'red'}} />)
      }
      var display_gender = true
    } else {
      var display_gender = false
    }

    if(this.state.vote && this.props.user.status != 'died') {
      var vote = true
    }

    if(this.state.bite && this.props.user.status != 'died' && this.props.user.role != '狼人') {
      var bite = true
    }

    if(this.state.guard && this.props.user.status != 'died' && this.props.user.role != '獵人') {
      var guard = true
    }

    if(this.state.divine && this.props.user.status != 'died' && this.props.user.role != '先知') {
      var divine = true
    }

    if(this.props.user.status != 'died') {
      var head = 'head'
    } else {
      var head = 'dead'
    }

    if(this.state.gameover) {
      var seeAll = true
    }

    if(this.props.userRole === '狼人') {
      if(this.props.user.role === '狼人') {
        var wolfHint = 'wolfHint'
      }
    }

    if(this.props.userRole === '狼之子' && this.props.computer === 'died') {
      if(this.props.user.role === '狼人') {
        var wolfHint = 'wolfHint'
      }
    }


    return(
      <div className={`col-md-3 villagerWrap`}>
        <div className={`card ${wolfHint}`}>
          <div className={`card-top`}>
            <div className={`${head}`} />
              <div>
                {
                  seeAll ?
                  <div className={`vote`}>
                    {this.props.user.role}
                  </div>
                : null
                }
                {
                  this.props.user.vote ?
                  <div className={`check`}>
                    <span className={`checkTooltip`}>
                      {
                        this.props.user.check ?
                        this.props.user.vote
                        : 'ok!'
                      }
                    </span>
                  </div>
                  : null
                }
                {
                  vote ?
                  <span
                    className={`vote`}
                    onClick={() => this.handleClick(1)}
                    >
                    vote
                  </span>
                  : null
                }
              </div>
            {
              bite ?
              <span
                className={`night-button`}
                onClick={() => this.handleClick(2)}
              >
                bite
              </span>
            : null
            }
            {
              divine ?
              <span
                className={`night-button`}
                onClick={() => this.handleClick(3)}
              >
                <i className="fa fa-eye" aria-hidden="true" />
              </span>
            : null
            }
            {
              guard ?
              <span
                className={`night-button`}
                onClick={() => this.handleClick(4)}
              >
                guard
              </span>
            : null
            }
            {
              this.props.user.ready ?
              <span className={`ready-button`} >
                Ready
              </span>
            : null
            }
          </div>

          <div className="card-text">
            {
              display_gender ? gender
              : <i className="fa fa-user" aria-hidden="true" />
            }
            &nbsp;&nbsp;&nbsp;
            {this.props.user.name}
          </div>
          <div className="card-text">
            {
              this.props.user.status === 'live' ?
              <i
                className="fa fa-heartbeat"
                aria-hidden="true"
                style={{color: '#cc181e'}}
              />
            : <i className="fa fa-heartbeat" aria-hidden="true" />
            }
            &nbsp;&nbsp;&nbsp;
            {this.props.user.status}
          </div>
        </div>

      </div>
    )
  }
}
