import React, { Component } from 'react';
import './Character.scss'

export default class Character extends Component {
  constructor() {
    super()
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      ready: false
    }
  }

  handleClick() {
    this.setState({
      ready: !this.state.ready
    })

    this.props.socket.emit('ready')
  }

  render() {
    var readyActive = this.state.ready? 'active' : null
    if ('role' in this.props.userData) {
      var roleDisplay = true
      switch (this.props.userData.role) {
        case "村民":
          var cardFront = 'villagerCard'
          break;
        case "先知":
          var cardFront = 'prophetCard'
          break;
        case "獵人":
          var cardFront = 'hunterCard'
          break;
        case "狼人":
          var cardFront = 'wolfCard'
          break;
        case "靈媒":
          var cardFront = 'psychicCard'
          break;
        case "妖狐":
          var cardFront = 'foxCard'
          break;
        case "狼之子":
          var cardFront = 'wolfSonCard'
          break;
        case "巫女":
          var cardFront = 'witchCard'
          break;
      }
    }

    if(this.props.userData.sex != null) {
      if(this.props.userData.sex) {
        var gender = (<i className="fa fa-mars" aria-hidden="true" style={{color: '#2626a6'}} />)
      } else {
        var gender = (<i className="fa fa-venus" aria-hidden="true" style={{color: 'red'}} />)
      }
      var genderDisplay = true
    } else {
      var genderDisplay = false
    }

    if(this.props.userData.status != null && this.props.userData.status === 'died') {
      var deadCard = 'deadCard'
    }

    return (
      <div className={`characterWrap`}>
        <div className={`roleCard ${deadCard}`}>
          {
            roleDisplay ?
            <div className={`flipperWrap`}>
              <div className={`flipper`}>
                <div className={`cardBack`} />
                <div className={`cardFront ${cardFront}`} />
              </div>
            </div>
          : <div className={`flipperWrap`}>
              <div className={`cardBack`} />
            </div>
          }
        </div>
        <div className={`info`}>
          {
            roleDisplay ?
            <div className={`basic`}>
              <span className={`name`}>
                {
                  genderDisplay ? gender
                : <i className="fa fa-user" aria-hidden="true" />
                }
                &nbsp;&nbsp;&nbsp;
                {this.props.userData.name}
              </span>
              <span>
                {
                  this.props.userData.status === 'live' ?
                  <i
                    className="fa fa-heartbeat"
                    aria-hidden="true"
                    style={{color: '#cc181e'}}
                  />
                : <i className="fa fa-heartbeat" aria-hidden="true" />
                }
                &nbsp;&nbsp;&nbsp;
                {this.props.userData.status}
              </span>
            </div>
          : <div className={`basic`}>
              <span className={`name`}>
                {this.props.userName}
              </span>
              <span
                className={`ready ${readyActive}`}
                onClick={this.handleClick}
                >
                Ready
              </span>
            </div>
          }
        </div>
      </div>
    )
  }
}
