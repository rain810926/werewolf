import React, { Component } from 'react';
import './RoleSetting.scss'

export default class RoleSetting extends Component {

  render() {
    return(
      <div className={`roleSettingWrap`}>
        <h4>角色配置</h4>
        <hr/>
        <table className="table table-bordered">
          <thead>
            <tr>
              <th>人數\</th>
              <th>村民</th>
              <th>先知</th>
              <th>靈媒</th>
              <th>獵人</th>
              <th>巫女</th>
              <th className={`wolfHint`}>狼人</th>
              <th className={`wolfHint`}>狼之子</th>
              <th>妖狐</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>6</td>
              <td>3</td>
              <td>1</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td className={`wolfHint`}>1</td>
              <td className={`wolfHint`}>1</td>
              <td>0</td>
            </tr>
            <tr>
              <td>7</td>
              <td>3</td>
              <td>1</td>
              <td>1</td>
              <td>0</td>
              <td>0</td>
              <td className={`wolfHint`}>2</td>
              <td className={`wolfHint`}>0</td>
              <td>0</td>
            </tr>
            <tr>
              <td>8</td>
              <td>4</td>
              <td>1</td>
              <td>1</td>
              <td>0</td>
              <td>0</td>
              <td className={`wolfHint`}>2</td>
              <td className={`wolfHint`}>0</td>
              <td>0</td>
            </tr>
            <tr>
              <td>9</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td className={`wolfHint`}>0</td>
              <td className={`wolfHint`}>0</td>
              <td>0</td>
            </tr>
            <tr>
              <td>10</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td className={`wolfHint`}>0</td>
              <td className={`wolfHint`}>0</td>
              <td>0</td>
            </tr>
            <tr>
              <td>11</td>
              <td>4</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td>0</td>
              <td className={`wolfHint`}>2</td>
              <td className={`wolfHint`}>1</td>
              <td>1</td>
            </tr>
            <tr>
              <td>12</td>
              <td>5</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td>0</td>
              <td className={`wolfHint`}>2</td>
              <td className={`wolfHint`}>1</td>
              <td>1</td>
            </tr>
            <tr>
              <td>13</td>
              <td>6</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td>0</td>
              <td className={`wolfHint`}>3</td>
              <td className={`wolfHint`}>0</td>
              <td>1</td>
            </tr>
            <tr>
              <td>14</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td>0</td>
              <td className={`wolfHint`}>0</td>
              <td className={`wolfHint`}>0</td>
              <td>0</td>
            </tr>
            <tr>
              <td>15</td>
              <td>7</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td className={`wolfHint`}>3</td>
              <td className={`wolfHint`}>1</td>
              <td>1</td>
            </tr>
            <tr>
              <td>16</td>
              <td>8</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td>1</td>
              <td className={`wolfHint`}>3</td>
              <td className={`wolfHint`}>1</td>
              <td>1</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}
