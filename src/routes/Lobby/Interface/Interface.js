import React, { Component } from 'react';
import './Interface.scss'
import Character from './Character'
import Village from './Village'
import GameRule from './GameRule'
import RoleIntro from './RoleIntro'
import RoleSetting from './RoleSetting'

export default class Interface extends Component {
  constructor() {
    super()
    this.state = {
      userData: {},
      userID: null,
      subInterface: 'village'
    }
    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount() {
    this.props.socket.on('update', (data) => {
      if(!data[1].role) {
        delete data[1]['role']
      }
      this.setState({
        userData: data[1],
        userID: data[0]
      })
    })
  }

  handleClick(type) {
    this.setState({
      subInterface: type
    })
  }


  render() {
    if(!this.state.userData.role) {
      var userRole = ''
    } else {
      var userRole = this.state.userData.role
    }

    switch (this.state.subInterface) {
      case 'village':
        var village = 'display'
        var other = 'none'
        break
      case 'gameRule':
        var village = 'none'
        var other = 'display'
        var subInterface = (<GameRule/>)
        break
      case 'roleIntro':
        var village = 'none'
        var other = 'display'
        var subInterface = (<RoleIntro/>)
        break
      case 'roleSetting':
        var village = 'none'
        var other = 'display'
        var subInterface = (<RoleSetting/>)
        break
    }


    return (
      <div className={`jumbotrun interfaceWrap`}>

        <div className={`col-md-3 character`}>
          <Character
            socket={this.props.socket}
            userData={this.state.userData}
            userName={this.props.userName}
          />
        </div>

        <div className={`col-md-9 ${village}`}>
          <Village
            userRole={userRole}
            socket={this.props.socket}
            userID={this.state.userID}
          />
        </div>

        <div className={`col-md-9 ${other}`}>
          {subInterface}
        </div>

        <div className="btnsWrap">
          <div
            className={`myBtn`}
            onClick={ () => {this.handleClick('village')} }
          >
            <div>村莊</div>
            <span className={`focus-border`} />
          </div>
          <div
            className={`myBtn`}
            onClick={ () => {this.handleClick('gameRule')} }
          >
            <div>遊戲規則</div>
            <span className={`focus-border`} />
          </div>
          <div
            className={`myBtn`}
            onClick={ () => {this.handleClick('roleIntro')} }
          >
            <div>角色介紹</div>
            <span className={`focus-border`} />
          </div>
          <div
            className={`myBtn`}
            onClick={ () => {this.handleClick('roleSetting')} }
          >
            <div>角色配置</div>
            <span className={`focus-border`} />
          </div>
        </div>
      </div>
    )
  }
}
