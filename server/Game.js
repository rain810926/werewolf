import UserData from './UserData'
import fs from 'fs'
var RoleSetting = JSON.parse(fs.readFileSync('./static/RoleSetting.json'))
var DAYTIME = 0
var NIGHTTIME = 0
var FIRSTTIME = 3
var EVENINGTIME = 0
var INTERVAL = 300


export default class Game {
  constructor(socket_io, sub_io,roomId) {
    console.log("[GAME]:: Game server start!")
    this.sSocket = require('socket.io-client')('http://localhost:3000')
    this.socket_io = socket_io
    this.sub_io = sub_io
    this.roomName = roomId
    this.players = {}
    this.playerNum = 0
    this.socketMap= {} //socketId to socket
    this.days = 0
    this.status = 'WAIT'
    this.minPlayers = 6
    this.wolfs = []
    this.gameover = false
    this.peace = true
    this.winer = null
    this.prophet = null
    this.psychic = null
    this.hunter = null
    this.fox = null
    this.injured = null
    this.aside1 = ''
    this.aside2 = ''
    this.testament = []

    // need to be refresh
    this.newDied = [] // the people died yesterday
    this.fire = null // 火刑
    this.bited = null
    this.biter = null
    this.guard = null
    this.divine = null
    this.vote = 0

    //computer
    this.players['computer'] = new UserData()
    this.players['computer'].name = '我是替身'
    this.players['computer'].role = '村民'
    this.players['computer'].ready = true
    this.players['computer'].sex = 1

    sub_io.on('connection', (socket) => {

      socket.on('come', (nickName) => {
        this.player_come(socket, nickName)
      })

      socket.on('chat', (text) => {
        this.handle_message(socket, text)
      })

      socket.on('ready', () => {
        this.handle_ready(socket)
      })

      socket.on('disconnect', () =>  {
        this.player_leave(socket)
      })

      socket.on('vote', (socketId) => {
        this.receive_vote(socket, socketId)
      })
      socket.on('bite', (socketId) => {
        this.receive_bite(socket, socketId)
      })
      socket.on('guard', (socketId) => {
        this.receive_guard(socket, socketId)
      })
      socket.on('divine', (socketId) => {
        this.receive_divine(socket, socketId)
      })

    })
  }

  game_refresh(type) {
    switch (type) {
      case 'day':
        this.sub_io.emit('guard', false)
        this.sub_io.emit('bite', false)
        this.sub_io.emit('divine', false)
        this.days += 1
        break;
      case 'night':
        this.newDied = [] // the people died yesterday
        this.bited = null // refresh
        this.biter = null // refresh
        this.guard = null // refresh
        this.divine = null // refresh
        this.vote = 0
        this.testament = []
        for(var key in this.players) {
          this.players[key].vote = null
          this.players[key].check = false
        }
        break
    }

    this.updateAll()
  }

  game_start() {
    console.log('[GAME]:: Game start!')
    this.aside(this.sub_io, 0, INTERVAL, false, [
      this.font_color('force', '注意！！'),
      '和平的白天持續 ' + FIRSTTIME + ' 秒',
      '其餘白天村民們必須進行投票',
      '全部人投票才會進入傍晚',
      '傍晚持續 12 秒進入黑夜',
      '黑夜須待狼人行動結束才會迎來晨曦'
    ], () => {
      setTimeout(() => {
        this.updateAll()
        this.transform('FIRST')
      }, 1000 * 5)
    })


    this.updateAll()
  }

  transform(status) {
    console.log('[GAME]:: Game status transform => ' + status)
    if(this.gameover) {
      return
    }

    switch(status) {
      case 'FIRST':
        this.sub_io.emit('transform')
        this.status = status
        this.sub_io.emit('time', FIRSTTIME)
        this.handle_first()
        break
      case 'DAY':
        this.sub_io.emit('transform')
        this.status = status
        this.handle_day()
        break
      case 'INIT':
        this.sub_io.emit('transform')
        this.status = status
        break
      case 'EVENING':
        this.status = status
        this.handle_evening()
        break
      case 'NIGHT':
        this.status = status
        this.sub_io.emit('transform')
        this.handle_night()
        // this.sub_io.emit('time', NIGHTTIME)
        break
      case 'CHECK':
        this.sub_io.emit('transform')
        this.status = status
        this.handle_check()
    }
  }

  handle_check() {
    this.sub_io.emit('vote', false)

    console.log('[GAME]:: Check the vote')
    var votes = []
    for(var key in this.players) {
      if(this.players[key].vote) {
        var vote = this.players[key].vote
        this.players[key].check = true
        votes.push(vote)
        this.players[key].vote = this.players[vote].name
      }
    }

    this.updateAll()

    var candidate = this.find_majority(votes)

    if(candidate.length === 1) {
      var id = candidate[0]
      this.fire = id
      this.aside(this.sub_io, 0, INTERVAL, false, [
        '開票結果出爐',
        this.font_color('name', this.players[id].name) + ' 獲得最高票',
        this.font_color('name', this.players[id].name) + ' 將被處死'
      ], () =>{
        setTimeout(() => {
          this.transform('EVENING')
        }, 1000 * 3)
      })
    } else {
      this.multiple_candidate(candidate)
    }

  }

  multiple_candidate(candidate) {
    console.log('[GAME]:: Multiple candidate')
    var tmp = ''
    for(var c in candidate) {
      tmp = tmp + ' ' +this.font_color('name', this.players[candidate[c]].name)
    }

    var died = this.random('select', candidate.length)
    var id = candidate[died]
    this.fire = id
    this.aside(this.sub_io, 0, INTERVAL, false, [
      '開票結果出爐',
      tmp + ' 獲得同票',
      '村長決定將 ' + this.font_color('name', this.players[id].name) + ' 處死'
    ], () => {
      setTimeout(() => {
        this.transform('EVENING')
      }, 1000 * 5)
    })
  }

  find_majority(votes) {
    var frequency = {}  // array of frequency.
    var max = 0  // holds the max frequency.
    var candidate = []   // holds the max frequency element.
    for(var v in votes) {
      frequency[votes[v]]=(frequency[votes[v]] || 0)+1 // increment frequency.
      if(frequency[votes[v]] > max) { // is this frequency > max so far ?
        candidate = []
        max = frequency[votes[v]]  // update max.
        candidate.push(votes[v])          // update candidate.
      } else if (frequency[votes[v]] === max) {
        candidate.push(votes[v])
      }
    }

    return candidate
  }

  start_vote() {
    for(var key in this.players) {
      if(this.players[key].status === 'died' || key === 'computer') {
        this.vote += 1
      }
    }
    for(var key in this.players) {
      if(key === 'computer') {
        continue
      }
      if(this.players[key].status != 'died') {
        this.socketMap[key].emit('vote', true)
      }
    }
    console.log('[GAME]:: Start to vote => ' + this.vote)
  }

  substitute(type, socketId) {
    this.aside(this.socketMap[socketId], 0, 0, false, [
      this.font_color('force', '替身救了你一命')
    ])
    switch (type) {
      case 'bite':
        switch (this.players[socketId].role) {
          case "先知":
          this.prophet = null
          break;
          case "靈媒":
          this.psychic = null
          break
        }
        this.players['computer'].role = this.players[socketId].role
        this.players[socketId].role = '村民'
        this.bited = 'computer'
        break
      case 'divine':
        this.players['computer'].role = this.players[socketId].role
        this.players[socketId].role = '村民'
        break
      case 'fire':
        this.players['computer'].role = this.players[socketId].role
        this.players[socketId].role = '村民'
        this.fire = 'computer'
        break
    }

    this.updateAll()
  }

  check_night() {
    if(this.bited != this.guard && this.players[this.bited].role != '妖狐') {
      if(this.bited != 'computer' && this.players['computer'].status != 'died') {
        this.substitute('bite', this.bited)
      }
      this.players[this.bited].status = 'died'
      this.testament.push(this.players[this.bited].name)
      this.newDied.push(this.bited)
      if(this.bited != 'computer') {
        this.socketMap[this.bited].emit('died')
      }


      if(this.players[this.bited].role === "巫女") {
        this.players[this.biter].status = 'died'
        this.socketMap[this.biter].emit('died')
        this.testament.push(this.players[this.biter].name)
        this.newDied.push(this.biter)
      }
    } else {
      console.log('[GAME]:: Someone was injured!')
      if(this.bited === this.guard) {
        if(this.random('select', 2)) {
          this.injured = this.biter
        } else {
          this.injured = this.hunter
        }
      } else if (this.players[this.bited].role === '妖狐') {
        if(this.random('select', 2)) {
          this.injured = this.biter
        } else {
          this.injured = this.fox
        }
        this.socketMap[this.biter].emit('bigPoster', [
          '你咬到了妖狐'
          // this.font_color('force', '你受傷了')
        ])
      }
      this.socketMap[this.injured].emit('bigPoster', [
        this.font_color('force', '你受傷了')
      ])
    }

    // 確認妖狐有沒有被咒殺
    if(this.divine != null && this.players[this.divine].role === '妖狐' && this.players[this.prophet].status != 'died') {
      if(this.players['computer'].status != 'died') {
        this.substitute('divine', this.divine)
        this.players['computer'].status = 'died'
        this.newDied.push('computer')
      } else {
        this.players[this.divine].status = 'died'
        this.newDied.push(this.divine)
        this.testament.push(this.players[this.divine].name)
        this.socketMap[this.divine].emit('died')
      }
    }


    this.updateAll()
    var deadBody = ''
    for(var key in this.newDied) {
      deadBody += ' ' + this.font_color('name', this.players[this.newDied[key]].name)
      if(this.newDied[key] != 'computer') {
        this.socketMap[this.newDied[key]].emit('bigPoster', [
          this.font_color('force', '你已經死了'),
          this.font_color('force', '今天結束以前'),
          this.font_color('force', '你的第一句話，將成為你的遺言'),
        ])
      }
    }

    if(this.newDied.length != 0) {
      this.peace = false
      this.aside1 = '發現' + deadBody + ' 的屍體'
      this.aside2 = '死狀非常悽慘'
    } else {
      this.aside1 = '昨晚聽見狼人的呼聲'
      this.aside2 = '所幸沒有人死亡'
    }

    return true
  }

  check_victory() {
    var wolfNum = 0
    var humanNum = 0
    var isFoxAlive = false

    for(var key in this.players) {
      if(this.players[key].status != 'died') {
        switch (this.players[key].role) {
          case '狼人':
            wolfNum += 1
            break;
          case '妖狐':
            isFoxAlive = true
            humanNum += 1
            break;
          default:
            humanNum += 1
        }
      }
    }

    if(wolfNum >= humanNum) {
      this.winer = '狼人'
      this.gameover = true
    } else if(wolfNum === 0 && isFoxAlive) {
      this.winer = '妖狐'
      this.gameover = true
    } else if(wolfNum === 0 && !isFoxAlive) {
      this.winer = '人類'
      this.gameover = true
    }

    if(this.gameover) {
      this.status = 'GAMEOVER'
      for(var key in this.players) {
        this.players[key].check = false
      }
      this.updateAll
      this.sub_io.emit('gameover', this.winer)
      return true
    } else {
      return false
    }
  }

  roleAbility_effect() {
    // for prophet
    if(this.prophet != null && this.players[this.prophet].status != 'died' && this.divine != null) {
      if(this.players[this.divine].role != '狼人') {
        this.socketMap[this.prophet].emit('bigPoster', [
          this.font_color('name', this.players[this.divine].name) + ' 是人類'
        ])
      } else {
        this.socketMap[this.prophet].emit('bigPoster', [
          this.font_color('name', this.players[this.divine].name) + ' 是狼人'
        ])
      }
    }
    // for psychic
    if(this.psychic != null && this.players[this.psychic].status != 'died' && this.fire != null) {
      this.socketMap[this.psychic].emit('bigPoster', [
        this.font_color('name', this.players[this.fire].name) + ' 是' + this.players[this.fire].role
      ])
      this.fire = null
    } else {
      this.fire = null
    }
  }

  handle_day() {
    this.sub_io.emit('day')

    if(this.check_night()) {
      this.roleAbility_effect()
    }

    if(this.peace) {
      this.transform('FIRST')
    } else {
      this.game_refresh('day')
      this.aside(this.sub_io, INTERVAL, INTERVAL, false, [
        '[第 ' + this.days + ' 日]',
        this.aside1,
        this.aside2,
        '眾人議論紛紛, 決定處死一名可疑人物'
      ], () => {
        if(!this.check_victory()) {
          this.start_vote()
        }
      })
    }

  }

  handle_night() {
    this.sub_io.emit('night')
    this.aside(this.sub_io, 0, 0, false, [
      '黑夜來臨',
    ])

    for(var key in this.players) {
      if(key === 'computer') {
        continue
      }
      if(this.players[key].status != 'died') {
        switch (this.players[key].role) {
          case '狼人':
            this.aside(this.socketMap[key], 0, INTERVAL, false, [
              '狼人請現身',
              '請在天亮前咬一個人',
              this.font_color('force', '注意！！'),
              this.font_color('force', '只要其中一名狼人咬下, 則不可更換目標')
            ])
            break;
          case '先知':
            this.aside(this.socketMap[key], 0, INTERVAL, false, [
              '先知請現身',
              '利用占卜查看一名目標的身份',
              '白天才能得到結果'
            ])
            this.socketMap[key].emit('divine', true)
            break;
          case '獵人':
            this.aside(this.socketMap[key], 0, 0, false, [
              '獵人請現身',
              '請選擇保護一名目標',
              '使其不受狼人威脅'
            ])
            this.socketMap[key].emit('guard', true)
            break
          default:
            this.aside(this.socketMap[key], 0, 0, false, [
              '你在床上呼呼大睡'
            ])
        }
      } else {
        this.aside(this.socketMap[key], 0, 0, false, [
          this.font_color('force', '你已經死了')
        ])
      }
    }

    setTimeout(() => {
      for(var index in this.wolfs) {
        if(this.players[this.wolfs[index]].status != 'died') {
          this.socketMap[this.wolfs[index]].emit('bite', true)
        }
      }
    }, 1000 * 5)
  }

  handle_evening() {
    this.fire_penalty()

    if(!this.check_victory()) {
      this.aside(this.sub_io, 1000 * 7, 0, false, [
        '快要天黑了',
      ], () => {
        this.game_refresh('night')
        setTimeout(() => {
          this.transform('NIGHT')
        }, 5000)
      })
    }

  }

  fire_penalty() {
    if(this.players['computer'].status != 'died') {
      this.substitute('fire', this.fire)
    }
    this.players[this.fire].status = 'died'

    if(this.fire != 'computer') {
      this.aside(this.socketMap[this.fire], 0, INTERVAL, true, [
        '已經傍晚了',
        '村民們將 ' + this.font_color('name', this.players[this.fire].name) + ' 逼上火刑台',
        '大火燃燒 ' + this.font_color('name', this.players[this.fire].name) + ' 發出淒厲的慘叫聲',
        this.font_color('name', this.players[this.fire].name) + ' 被活活燒死了'
      ])

      this.aside(this.socketMap[this.fire], 0, INTERVAL, false, [
        '已經傍晚了',
        '村民們將你逼上火刑台',
        '大火燃燒, 你痛苦的哀嚎著',
        '你被活活燒死了'
      ], () => {
        this.socketMap[this.fire].emit('died')
        this.updateAll()
        this.check_victory()
      })
    } else {
      this.aside(this.sub_io, 0, INTERVAL, false, [
        '已經傍晚了',
        '村民們將 ' + this.font_color('name', this.players[this.fire].name) + ' 逼上火刑台',
        '大火燃燒 ' + this.font_color('name', this.players[this.fire].name) + ' 發出淒厲的慘叫聲',
        this.font_color('name', this.players[this.fire].name) + ' 被活活燒死了'
      ])
    }

  }

  handle_first() {
    this.game_refresh('day')
    this.game_refresh('night')
    this.aside(this.sub_io, 0, INTERVAL, false, [
      '[第 ' + this.days + ' 日]',
      '今天是和平的一天',
      '村民們開心的生活著'
    ])

    setTimeout(() => {
      this.aside(this.sub_io, 0, INTERVAL, false, [
        '快要天黑了'
      ])
    }, 1000 * (FIRSTTIME - 5))

    setTimeout(() => {
      this.transform('NIGHT')
    }, 1000 * FIRSTTIME)
  }

  receive_vote(voter, elected) {
    if(this.status === 'DAY' && this.players[voter.id].status != 'died' && this.players[voter.id].vote === null) {
      this.players[voter.id].vote = elected
      this.vote += 1
      console.log('[GAME]:: Receive a vote => ' + this.vote)
      voter.emit('vote', false)
      this.updateAll()
    }

    if(this.vote >= this.playerNum) {
      this.transform('CHECK')
    }
  }

  receive_bite(biter, bited) {
    if(this.status === 'NIGHT' && this.bited === null && this.players[bited].status != 'died') {
      this.bited = bited
      this.biter = biter.id
      this.aside(this.sub_io, 0, 0, false, [
        '快要天亮了',
      ], () => {
        this.sub_io.emit('time', 8)
        setTimeout(() => {
          this.transform('DAY')
        }, 1000 * 8)
      })
      for(var key in this.wolfs) {
        this.socketMap[this.wolfs[key]].emit('bite', false)
        this.aside(this.socketMap[this.wolfs[key]], 0, 0, false, [
          this.font_color('name', this.players[this.biter].name) + ' 咬了 ' + this.font_color('name',this.players[this.bited].name)
        ])
      }
    }
  }

  receive_guard(socket, guard) {
    if(this.status === 'NIGHT' && this.guard === null && this.players[guard].status != 'died') {
      socket.emit('guard', false)
      this.guard = guard
    }
  }

  receive_divine(socket, divine) {
    if(this.status === 'NIGHT' && this.divine === null && this.players[divine].status != 'died') {
      socket.emit('divine', false)
      this.divine = divine
    }
  }

  updateAll() { //updateAll
    var users = JSON.parse(JSON.stringify(this.players))
    for(var key in this.players) {
      if(key === 'computer') {
        continue
      }
      this.socketMap[key].emit('update', [key, this.players[key]])
    }

    this.sub_io.emit('updateAll', users)
  }

  game_init() {
    // 5人局： [3]獵人 將變為村民
    // 12,13,14人局： [9]狼孩子 將變為狼
    console.log('[GAME]:: Game initializing')
    this.transform('INIT')
    this.aside(this.sub_io, 0, 0, false, [
      '遊戲即將開始, 請稍候 ...'
    ])

    var roleNumber = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16"]
    this.playerNum = Object.keys(this.players).length
    roleNumber = roleNumber.slice(0, this.playerNum - 1) //扣掉替身
    this.shuffle(roleNumber)

    switch(this.playerNum - 1) { //扣掉替身
      case 5:
        RoleSetting["4"] = "狼之子"
        break

      case 8: //妖狐獵人隨機
        if(this.random('select', 2)) {
          RoleSetting["8"] = "獵人"
          RoleSetting["7"] = "狼之子"
        } else {
          RoleSetting["8"] = "妖狐"
        }
        break
      case 9: //妖狐獵人隨機
        if(this.random('select', 2)) {
          RoleSetting["8"] = "獵人"
          RoleSetting["7"] = "狼之子"
        } else {
          RoleSetting["8"] = "妖狐"
        }
        break
      case 10:
        RoleSetting["9"] = "狼之子"
        break
      case 12:
        RoleSetting["11"] = "狼人"
        break
    }

    var index = 0
    var tmpSocket
    // var leader = this.random('select', this.playerNum) //扣掉替身
    for (var key in this.players) {
      if(key === 'computer') {
        this.players[key].ready = false
        continue
      }
      this.players[key].ready = false
      this.players[key].role = RoleSetting[roleNumber[index]]
      // if (index === leader) {
      //   this.players[key].leader = true
      // }

      // determine sex
      if (this.random('confirm', 4)) {
        this.players[key].sex = 0
      } else {
        this.players[key].sex = 1
      }

      switch (RoleSetting[roleNumber[index]]) {
        case '巫女':
          this.players[key].sex = 0
          break;
        case '狼人':
          this.wolfs.push(key)
          break;
        case '先知':
          this.prophet = key
          break;
        case '先知':
          this.prophet = key
          break;
        case '靈媒':
          this.psychic = key
          break;
        case '獵人':
          this.hunter = key
          break;
        case "妖狐":
          this.fox = key
          break;
      }

      index += 1

      this.socketMap[key].emit('update', [key, this.players[key]])
    }

    DAYTIME = 10//this.playerNum * 15 + 90
    NIGHTTIME = 10//this.wolfs.length * 10 + 20
    EVENINGTIME = 10

    this.game_start()
  }

  handle_ready(socket) {
    if (this.status === 'WAIT') {
      this.players[socket.id].ready = !this.players[socket.id].ready

      if(this.players[socket.id].ready) {
        socket.emit('aside', 'You are ready!')
      } else {
        socket.emit('aside', 'You are not ready!')
      }

      this.updateAll()
    }
    var count = 0
    for(var key in this.players) {
      if (!this.players[key].ready) {
        return
      }
      count += 1
    }

    if (count >= this.minPlayers) {
      this.sSocket.emit('roomClose')
      this.game_init()
    }
  }

  player_come(socket, nickName) {
    console.log("[GAME]:: A new villager comes!")
    this.players[socket.id] = new UserData()
    this.players[socket.id].name = nickName
    this.socketMap[socket.id] = socket

    this.aside(socket, 0, INTERVAL, true, [
      this.font_color('name', nickName) + ' 來到這個村莊',
      '現在有 ' + Object.keys(this.players).length + ' 個村民'
    ])

    this.aside(socket, 500, INTERVAL, false, [
      '哈囉, ' + this.font_color('name', nickName) + ' !',
      '現在有 ' + Object.keys(this.players).length + ' 個村民'
    ])
    this.updateAll()
  }

  player_leave(socket) {
    if (this.status === 'WAIT') {

      if(this.players[socket.id]) {
        console.log("[GAME]:: A villager leaves")
        this.aside(this.sub_io, 0, INTERVAL, false, [
          this.players[socket.id].name + ' 離開了村莊',
          '現在有' + (Object.keys(this.players).length - 1) + ' 個村民'
        ], () => {
          delete this.players[socket.id]
          this.updateAll()
        })
        console.log("[GAME]:: A villager leaves")
      }

    } else {
      if(this.players[socket.id]) {
        console.log("[GAME]:: A villager died!")
        this.players[socket.id].status = 'died'
        this.updateAll()
      }
    }
  }

  aside(socket, start, interval, broadcast, messages, callback=null) {
    if(start != 0) {
      setTimeout(() => {
        this.aside(socket, 0, interval, broadcast, messages, callback)
      }, start)
    } else {
      var message = messages.shift();

      if(message) {
        if(broadcast) {
          socket.broadcast.emit('aside', message)
        } else {
          socket.emit('aside', message)
        }
        setTimeout(() => {
          this.aside(socket, 0, interval, broadcast, messages, callback)
        }, interval)
      } else {
        if(callback) {
          callback()
        }
      }
    }

  }

  handle_message(socket, message) {
    var player = this.players[socket.id]

    switch(this.status) {
      case 'WAIT':
        this.chat(player.status,{
          owner: player.name,
          text: message
        })
        break
      case 'GAMEOVER':
        this.chat(player.status,{
          owner: player.name,
          text: message
        })
        break
      case 'INIT':
        break
      case 'DAY':
        if(this.injured === socket.id) {
          this.injured = null
          this.aside(this.sub_io, 0, 0, false, [
            this.font_color('name', player.name) + ' 似乎受傷了'
          ])
        }
        this.chat(player.status, {
          owner: player.name,
          text: message
        })
        break
      case 'EVENING':
        this.chat(player.status, {
          owner: player.name,
          text: message
        })
        break
      case 'FIRST':
        this.chat(player.status,{
          owner: player.name,
          text: message
        })
        break
      case 'NIGHT':
        var data = {
          owner: player.name,
          text: message
        }
        if(player.status === 'died') {
          this.ghost(data)
        } else if(player.role === '狼人') {
          this.howl(player.status, data)
        }
        break

    }
  }

  howl(status, data) {
    for(var key in this.players) {
      if(this.players[key].role === '狼人') {
        this.socketMap[key].emit('chat', data)
      }
    }
  }

  ghost(data) {
    if(this.testament.indexOf(data.owner) > -1) {
      this.testament = ''
      if(data.text.length > 20) {
        var tmp = data.text.slice(0, 20)
      } else {
        var tmp = data.text
      }
      if(this.status != 'NIGHT') {
        this.sub_io.emit('bigPoster', [
          '發現 ' + this.font_color('name', data.owner) + ' 的遺書：',
           this.font_color('force', tmp)
        ])
      }
      return
    }
    data['died'] = true
    for(var key in this.players) {
      if(key === 'computer') {
        continue
      }
      if(this.players[key].status === 'died') {
        this.socketMap[key].emit('chat', data)
      } else if (this.players[key].role === '靈媒') {
        if(this.random('confirm', 3)) {
          this.socketMap[key].emit('chat', data)
        }
      }
    }
  }

  chat(status,data) {
    if(status != 'died') {
      this.sub_io.emit('chat', data)
    } else {
      this.ghost(data)
    }
  }

  font_color(type, text) {
    switch (type) {
      case 'name':
        text = '<font color="#ff9626">' + text + '</font>'
        break;
      case 'force':
        text = '<font color="#ff0000">' + text + '</font>'
        break;
    }
    return text
  }

  random(type, num) {
    switch (type) {
      case 'select':
        var tmp = Math.floor((Math.random() * num))
        return tmp
        break;
      case 'confirm':
        var tmp = Math.floor((Math.random() * 10))
        if(tmp < num) {
          return true
        } else {
          return false
        }
        break;
    }
  }

  shuffle(array) {
    for (let i = array.length; i; i--) {
        let j = Math.floor(Math.random() * i);
        [array[i - 1], array[j]] = [array[j], array[i - 1]];
    }
  }

}
