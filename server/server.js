import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'
import UserData from './UserData'
import GameServer from './Game'

var isRoomExist = false
var serialNum = 0

var config = require('../webpack.config');
var webpack = require('webpack')
var app = express();
var server = require('http').Server(app);
var compiler = webpack(config);
var io = require('socket.io')(server)

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath,
  noInfo: true,
  quiet: false
}));

app.use(require('webpack-hot-middleware')(compiler));
app.use( bodyParser.json() );
app.use(express.static('static'));

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../index.html'));
});

app.post('/', function(req, res) {
  console.log('[SERVER]:: A players come')

  if (req.body.nickName.length < 1) {
    res.sendStatus(301)
  } else if(req.body.nickName.length > 20) {
    res.sendStatus(302)
  } else {

    if(!isRoomExist) {
      console.log('[SERVER]:: Create a new game')
      isRoomExist = true
      serialNum += 1
      var roomId = 'room' + serialNum
      var sub_io = io.of('/' + roomId)
      var gameServer = new GameServer(io, sub_io, roomId)
    } else {
      var roomId = 'room' + serialNum
    }
    res.status(200).send({room: roomId})
  }
});

io.on('connection', (socket) => {
  socket.on('roomClose', () => {
      isRoomExist = false
  })
})

server.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
})
