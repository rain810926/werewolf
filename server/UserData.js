
export default class UserData {
  constructor() {
    this.name = ''
    this.role = null
    this.leader = false
    this.sex = null
    this.status = 'live'
    this.vote = null
    this.check = false
    this.ready = false
  }
}
